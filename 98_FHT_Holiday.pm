#
# 99_FHT_Holiday.pm
# defines a switch to send one or more FHTs int holiday-mode
#
# 2014 Nils Andresen <nils@nils-andresen.de>
#

# start-of-template
package main;

use strict;
use warnings;
use POSIX;

my %sets = (
  "return" => "",
  "away" => ""
);

sub
FHT_Holiday_Initialize($$)
{
  my ($hash) = @_;

  #$hash->{InitFn}    = "FHT_Holiday_Init";
  $hash->{DefFn}     = "FHT_Holiday_Define";
  $hash->{SetFn}     = "FHT_Holiday_Set";
  #$hash->{GetFn}     = "FHT_Holiday_Get";
  #$hash->{UndefFn}   = "FHT_Holiday_Undef";
  #$hash->{AttrFn}    = "FHT_Holiday_Attr";
  #$hash->{StateFn}   = "FHT_Holiday_State";

  $hash->{AttrList}  = "return-high-default return-low-default short-holiday-limit long-holiday-temp " .
					   $readingFnAttributes;
}

sub
FHT_Holiday_Define($$)
{
  my ($hash, $def) = @_;
  my @a = split("[ \t][ \t]*", $def);
  if(@a > 3) {
    my $msg = "wrong syntax: define <name> FHT_Holiday {none | devicename}";
    Log3 undef, 2, $msg;
    return $msg;
  }

  my $name = $a[0];
  my $dev = "TYPE=FHT";
  if(@a > 2) {
    $dev = $a[2];
  }

  Log3 $name, 3, "FHT_Holiday created $name for $dev";

  $hash->{DEV} = $dev;
  $hash->{STATE} = "Initialized";
  
  return undef;
}

sub
FHT_Holiday_Set($@)
{
  my ($hash, @a) = @_;
  return "\"set FHT_Holiday\" needs at least one parameter" if(@a < 2);

  my $name = shift @a;
  my $type = shift @a;
  my $dev = $hash->{DEV};
  
  if(!defined($sets{$type})) {
    my $cmdList = join(" ",sort keys %sets);
    return "Unknown argument $type, choose one of $cmdList";
  }

  if($type eq "return") {
	return FHT_Holiday_SetReturn($name, $dev);
  } elsif($type eq "away") {
	return FHT_Holiday_SetAway($name, $dev, @a);
  }
  
  return "";
}

sub 
FHT_Holiday_SetAway($$$)
{
  my ($name, $dev, @args) = @_;
  my $short = AttrVal($name, "short-holiday-limit", 3.0);

  if(@args < 1 || @args > 2) {
    my $msg = "wrong syntax: set $name away <days> [<temp>]";
    Log3 $name, 2, $msg;
    return $msg;
  }

  my $days = shift @args;
  if($days < 1) {
    my $msg = "Rubbish: set $name away $days makes no sense. Please choose at least 1 day!";
    Log3 $name, 2, $msg;
    return $msg;
  }

  my $temp;
  if(@args > 1) 
  {
    $temp = shift @args; 
    return FHT_Holiday_SetThemAllAway($dev, $temp, $days);
  } else {
	  if($days le $short) {
        return FHT_Holiday_SetThemAllAwayShort($name, $dev, $days);
	  } else {
        $temp = AttrVal($name, "long-holiday-temp", "17.0");
        return FHT_Holiday_SetThemAllAway($dev, $temp, $days);
	  }
  }

  return "";
}

sub
FHT_Holiday_SetThemAllAway($$$)
{
	my ($dev, $temp, $days) = @_;
	Log3 "debug", 3, "$dev SetThemAllAway $days days to $temp"
}

sub
FHT_Holiday_SetThemAllAwayShort($$$)
{
  my ($name, $dev, $days) = @_; 
  foreach my $fht (devspec2array($dev)) {
    my $temp = AttrVal($name, "return-low-default", "18.0");
    $temp = ReadingsVal($fht, "night-temp", $temp);
    my @end = localtime(time + (68400 * $days));
    my $day = strftime("%d", @end);
    my $month = strftime("%m", @end);
    fhem("set $fht mode holiday holiday1 $day holiday2 $month desired-temp $temp");
  }
  return "";
}

sub 
FHT_Holiday_SetReturn($$)
{
	my ($name, $dev) = @_;
	my $high = AttrVal($name, "return-high-default", "21.0");
	my $low = AttrVal($name, "return-low-default", "18.0");
    foreach my $fht (devspec2array($dev)) {
      FHT_Holiday_FHTnominal($fht, $high, $low);
	}
	return "";
}

sub 
FHT_Holiday_FHTnominal($$$) {
  # frei kopiert von http://www.fhemwiki.de/wiki/FHT80b_Automatik_setzen
  # Solltemperatur (auto) eines FHT zur aktuellen Uhrzeit ermitteln
  my($fht, $high, $low) = @_;
  my $jetzt = strftime("%R", localtime(time));
  my @wdays = qw(sun mon tue wed thu fri sat);
  my $tag = $wdays[strftime("%w", localtime(time))];
  my $f1 = ReadingsVal($fht, $tag . "-from1", "00:00");
  my $f2 = ReadingsVal($fht, $tag . "-from2", "00:00");
  my $t1 = ReadingsVal($fht, $tag . "-to1", "24:00");
  my $t2 = ReadingsVal($fht, $tag . "-to2", "24:00");
  if(($jetzt ge $f1 && $jetzt le $t1) || ($jetzt ge $f2 && $jetzt le $t2)) {
    fhem("set $fht mode auto desired-temp " . ReadingsVal($fht, "day-temp", $high));
  } else {
    fhem("set $fht mode auto desired-temp " . ReadingsVal($fht, "night-temp", $low));
  }
}

1;

=pod
=begin html

<a name="FHT_Holiday"></a>
<h3>FHT_Holiday</h3>
<ul>
  allows to easily set one or more (or all) of the FHT's into holiday-mode using pre-defined 
  desired-temperatures.
  
  <a name="FHT_Holday_def"></a>
  <b>define</b>
  <ul>
    <code>define &lt;name&gt; FHT_Holiday [&lt;device&gt;]</code>
    <br/>
    <br/>
    creates a new FHT_Holdiay device which will manage the given &lt;device&gt;. If &lt;device&gt; is omitted, <code>TYPE=FHT</code> is used. 
  </ul>
</ul>

=end html
=cut
