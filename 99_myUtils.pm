# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
# IMPORTANT: do not insert your own functions inside
# the file 99_Utils.pm!
#
# This file will be overwritten during an FHEM update and all
# your own inserts will be lost.
# !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
#
# To avoid this, we recommend following procedure:
#
# 1. Create your own file 99_myUtils.pm from the template below, 
#    e.g. with FHEMWEB, Edit files, Editing 99_Utils.pm, and saving it as 
#    99_myUtils.pm
# 2. Put this file inside the ./FHEM directory
# 3. Put your own functions into this new file
#



# start-of-template
package main;

use strict;
use warnings;
use POSIX;

sub
myUtils_Initialize($$)
{
  my ($hash) = @_;
  Log3 "myUtils:init", 2, "myUtils: init: myUtils up and running."
}

##########################################################
# update_hm_autoUpdate
# prueft für HM-geräte ob autoReadReg kokrrekt gesetzt ist...
#
sub update_hm_autoUpdate() {
  Log3 "update_hm_autoUpdate", 1, "update_hm_autoUpdate: checking autoReadReg...";
  my @skip = ("ActionDetetor");
  my @devices = split(/^/, fhem("list TYPE=CUL_HM"));
  my $attr;
  my $device = "";
  foreach (@devices) {
    $device = $_;
    $device =~ s/^\s+|\s+$//g; #trim...
    if($device ~~ @skip) {
      Log3 "update_hm_autoUpdate", 1, "update_hm_autoUpdate: skipping $device (blacklisted)";
      next;
    }
    $attr = AttrVal($device, "autoReadReg", undef);
    if($attr ne "5_readMissing") {
      if($attr eq undef){
        Log3 "update_hm_autoUpdate", 1, "update_hm_autoUpdate: skipping $device (no reading)";
        next;
      }
      Log3 "update_hm_autoUpdate", 1, "update_hm_autoUpdate: setting autoReadReg on $device";
      fhem("attr $device autoReadReg 5_readMissing")
    }
  }
}

##########################################################
# send_mail
# versendet mails
#
sub send_mail($$$) {
  my ($rcpt, $subject, $text) = @_;

  system("/bin/echo \"$text\" | /usr/bin/mail -s \"$subject\" \"$rcpt\"");
}

##########################################################
# checkHmWarnings
# prüft, ob warnings an den HM vorliegen
# regelmäßige prüfung mit "at"
# define check_hmwarnings at *09:00:00 { checkHmWarnings("nils.und.diana\@gmail.com") }
#  
# 
sub checkHmWarnings($) {
  my ($email) = @_;
  #my $actors = ReadingsVal("hm.info", "I_actTotal", "dead:all");
  my $batteries = InternalVal("hm.info", "ERR_battery", "");
  #my %actorStatus;
  #my %batteryStatus;
  #while ($actors =~ /(?<name>[a-z]+):(?<val>[0-9]+),?/g ) {
  #  $actorStatus{$+{name}} = int($+{val});
  #}
  if($batteries ne "") {
    send_mail($email, "Batterie-Warnung: ".$batteries, $batteries);
  }

  # TODO: DO SOMETING?!
  return "";
}

##########################################################
# shutdownPrinter
# prüft, ob der drucker im Standby ist
# regelmäßige prüfung mit "at"
#  
# 
sub shutdownPrinter() {
  my $on = ReadingsVal("hm.sw.sw6_Sw", "state", "on");
  my $power = 0 + ReadingsVal("hm.sw.sw6_Pwr", "power", "0");

  if($on eq "on"){
    Log3 "myUtils:shutdownPrinter", 4, "Drucker läuft mit " . $power . "mW...";
    if($power < 5) {
      fhem("set hm.sw.sw6_Sw off");
    }
  }

  return 1;
}


#####################################
#
# Funktion, die Sonos überprüfungen ein- und ausschaltet
#  aufrufen mit: define check_sonos notify hm.sw.sw(3|4) { check_sonosAvailable() }
#
sub check_sonosAvailable() {
  my $wohnzimmer = ReadingsVal("hm.sw.sw3", "state", "on") eq "on";
  my $kueche = ReadingsVal("hm.sw.sw4", "state", "on") eq "on";
  my $sonosDisabled = defined AttrVal("sonos", "disable", undef);
  my $loglevel = 1;
  
  if(($wohnzimmer || $kueche) && $sonosDisabled){
    # boxen an, device ist aus..
    Log3 "myUtils:check_sonosAvailable", $loglevel, "check_sonosAvailable: set sonos enabled";
    fhem("deleteattr sonos disable");
  }
  if(!$wohnzimmer && !$kueche && !$sonosDisabled){
    # boxen aus, aber device noch an..
    Log3 "myUtils:check_sonosAvailable", $loglevel, "check_sonosAvailable: set sonos disabled";
    fhem("attr sonos disable 1");
  }
  return 1;
}

#####################################
#
# Funktion zum Püfen, ob die außenbeleuchtung eingeschaltet werden muss...
#  aufrufen in: do_sundown und bei abwesenheiten....
#  schaltet ein, wenn jemand awesend ist und der küchen-rolladen mehr als 50% geschlossen.
#
sub check_outerLight_on() {
  my $loglevel =1;
  Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: checking outerlight on needed.."; 
  my $roll = int(ReadingsVal("hm.eg.roll.kue", "pct", "0"));
  Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: shutter is $roll % open"; 
  my $isNight = 0;
  if($roll < 50) {
    Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: seems it is night..."; 
    $isNight = 1;
  } else {
    Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: seems it is day..."; 
  }

  my $residentCount = int(ReadingsVal("residents", "residentsTotal", "100"));
  my $residentsAbsent = int(ReadingsVal("residents", "residentsAbsent", "100"));
  Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: $residentsAbsent residents are absent (of $residentCount)"; 
  my $isSomeoneAway = 0;
  if($residentsAbsent > 0){
    Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: seems someone is away..."; 
    $isSomeoneAway = 1;
  }
  
  if($isNight && $isSomeoneAway) {
    Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: switching outerlight on!"; 
    fhem("set hm.eg.vorn on");
    return "on";
  } else {
    Log3 "myUtils:check_outerLight_on", $loglevel, "myUtils: check_outerLight_on: switching outerlight off!"; 
    fhem("set hm.eg.vorn off");
    return "off";
  }

  return 1;
}

#####################################
#
# Funktion bei Sonnenaufgang:
#  aufrufen mit: define at_sunrise at *{sunrise("REAL")} {do_sunrise()}
#
sub do_sunrise() {
   Log3 "myUtils:do_sunrise", 4, "doing sunrise...";
   # außenlichter aus
   fhem("set hm.eg.vorn off");
   # alle Rollaeden hoch
   my $up = 100;
   my @rollos = qw(hm.eg.roll.bad hm.eg.roll.kue hm.eg.roll.wohnzi hm.eg.roll.terrasse);
   for(my $i = 0; $i < @rollos; $i++) {
       my $fht = $rollos[$i];
       my $pct = ReadingsVal($fht, "pct", "-1");
       if(("".$pct) ne ("".$up)) {
           fhem("set " . $fht . " pct " . $up);
       } else {
           Log3 "myUtils:do_sunrise", 3, "myUtils: do_sunset: " . $fht . " already at pct:" . $up;
       }
   }
}

#####################################
#
# Funktion bei Sonnenuntergang:
#  aufrufen mit: define at_sunset at *{sunset("REAL")} {do_sundown()}
#
sub do_sundown() {
   my $loglevel = 1;
   Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: doing sunset...";
   my $down = 0;
   my $partlyDown = 30;
   my $tuer = ReadingsVal("hm.eg.tf.terr", "state", "open");
   my $fenster = ReadingsVal("hm.eg.tf.kue", "state", "open"); 

   fhem("set hm.eg.roll.bad pct $down");
   fhem("set hm.eg.roll.wohnzi pct $down");

   # kueche, nicht ganz runter wenn Fenster offen...
   Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: hm.eg.tf.kue reported state: $fenster";
   if($fenster eq "closed") {
      Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: setting pct for hm.eg.roll.kue to $down";
      fhem("set hm.eg.roll.kue pct $down");
   } else {
      Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: setting pct for hm.eg.roll.kue to $partlyDown";
      fhem("set hm.eg.roll.kue pct $partlyDown");
   }

   # terrassentuer, nur wenn die nicht auf ist...
   Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: hm.eg.tf.terr reported state: $tuer";
   if($tuer eq "closed") {
      Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: setting pct for hm.eg.roll.terrasse to $down";
      fhem("set hm.eg.roll.terrasse pct $down");
   } elsif($tuer eq "tilted") {
      $partlyDown = 35;
      Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: setting pct for hm.eg.roll.terrasse to $partlyDown";
      fhem("set hm.eg.roll.terrasse pct $partlyDown");
   } else {
      Log3 "myUtils:do_sunset", $loglevel, "myUtils: do_sunset: canceling close of hm.eg.roll.terrasse";
   }

   # außenlicht prüfen... (mit timeout...)
   fhem("define timed_outerlight at +00:01:20 {check_outerLight_on()}");
}

#####################################
#
# check_abluft:
#  prüft für alle Fenster im EG, ob die Abzuhgsaube an, oder aus sein soll.
# nofity mit: define check_abluft notify (hm.eg.tf.ht|hm.eg.tf.kue|hm.eg.tf.terr):contact:.*hm.virt.* { check_abluft($NAME, $EVENT) }
#
sub check_abluft($$) {
  my ($name, $event) = @_;
  my $loglevel = 2;
  Log3 "myUtils:check_abluft", 1, "myUtils: check_abluft: start mit: " . $name . ", evt:" . $event;
  my $kue = ReadingsVal("hm.eg.tf.kue", "state", "closed");
  my $ht = ReadingsVal("hm.eg.tf.ht", "state", "closed");
  my $terr = ReadingsVal("hm.eg.tf.terr", "state", "closed");
  my $abluft = ReadingsVal("hm.sw.sw2", "state", "off");
  my $isAbluft = 0;
  my $allowAbluft = 0;

  if($abluft ne "off") {
    $isAbluft = 1;
  }
  if($kue ne "closed" || $ht ne "closed" || $terr ne "closed") {
    $allowAbluft = 1;
  }

  if($isAbluft == $allowAbluft) {
    return 1;
  }

  Log3 "myUtils:check_abluft", 1, "myUtils: check_abluft: Kuechenfenster: " . $kue;
  Log3 "myUtils:check_abluft", 1, "myUtils: check_abluft: Haustür: " . $ht;
  Log3 "myUtils:check_abluft", 1, "myUtils: check_abluft: Terrasse: " . $terr;

  if($allowAbluft) {
    Log3 "myUtils:check_abluft", 1, "myUtils: check_abluft: Abluft: Ein";
    fhem("set hm.sw.sw2 on");
  } else {
    Log3 "myUtils:check_abluft", 1, "myUtils: check_abluft: Abluft: Aus";
    fhem("set hm.sw.sw2 off");
  }
  return 1;
}

#####################################
#
# check_windowShutters:
#  prüft, für Fenster den Status der Shutters:
#   Fenster war zu, ist jetzt auf -> Wenn Rolladen zu war, dann etwas ( ~ 10%) öffnen
#   Fenster war auf, ist jetzt zu -> Wenn Rolladen nur etwas auf war ( ~ 10%) dann öffnen 
# nofity mit: define check_windowShutters notify (hm.eg.tf.kue|hm.eg.tf.terr):contact:.*hm.virt.* { check_windowShutters($NAME, $EVENT) }
#
sub check_windowShutters($$) {
    my ($name, $event) = @_;
    my $target = "";
    my $newStatus = "Closed";
    my $open = 30;
    my $loglevel = 1;
    Log3 "myUtils:check_windowShutters", $loglevel, "check_windowShutters: called with name:$name and event:$event";
    if($name eq "hm.eg.tf.terr") {
        $target = "hm.eg.roll.terrasse";
        $open = 35;
        $newStatus = ($event =~ m/closed/) ? "Closed" : "Open";
    } elsif ($name eq "hm.eg.tf.kue") {
        $target = "hm.eg.roll.kue";
        $newStatus = ($event =~ m/closed/) ? "Closed" : "Open";
    } else {
        Log3 "myUtils:check_windowShutters", 1, "myUtils: check_windowShutters: name not valid: " . $name;
        return -99;
    }
    
    my $pos = int(ReadingsVal($target, "pct", "101"));
    if($pos > 100 || $pos < 0) {
        Log3 "myUtils:check_windowShutters", 1, "myUtils: check_windowShutters: position not valid for target: " . $target. "pct:" . $pos;
        return -99;
    }

    Log3 "myUtils:check_windowShutters", $loglevel, "check_windowShutters: shutter (" . $target . ") for winow (" . $name . ") is at pct:" . $pos. " while threshold is " . $open;
    if($newStatus eq "Closed") {
        if($pos <= $open && $pos > 0) {
            # window has been closed and shutter is "nearly" closed -> close shutter
            Log3 "myUtils:check_windowShutters", $loglevel, "myUtils: check_windowShutters: setting shutter (" . $target . ") for winow (" . $name . ") to pct:0";
            fhem("set " . $target . " pct 0");
        }
    } elsif($newStatus eq "Open"){
        if($pos < $open) {
            # window has been opened and shutter is more closed than required -> open shutter
            Log3 "myUtils:check_windowShutters", $loglevel, "myUtils: check_windowShutters: setting shutter (" . $target . ") for winow (" . $name . ") to pct:" . $open;
            fhem("set " . $target . " pct " . $open);
        }
    }
}

1;
